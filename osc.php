<?php
/**
 * 开源中国 API client for PHP
 *
 * @author PiscDong (http://www.piscdong.com/)
 */
class oscPHP
{
	public $api_url='https://www.oschina.net/action/openapi/';
	public $format='json';

	public function __construct($client_id, $client_secret, $access_token=NULL){
		$this->client_id=$client_id;
		$this->client_secret=$client_secret;
		$this->access_token=$access_token;
	}

	//生成授权网址
	public function login_url($callback_url){
		$params=array(
			'response_type'=>'code',
			'client_id'=>$this->client_id,
			'redirect_uri'=>$callback_url
		);
		return 'https://www.oschina.net/action/oauth2/authorize?'.http_build_query($params);
	}

	//获取access token
	public function access_token($callback_url, $code){
		$params=array(
			'grant_type'=>'authorization_code',
			'code'=>$code,
			'client_id'=>$this->client_id,
			'client_secret'=>$this->client_secret,
			'redirect_uri'=>$callback_url,
			'dataType'=>$this->format
		);
		return $this->api('token', $params);
	}

	/**
	//使用refresh token获取新的access token，开源中国暂时不支持
	public function access_token_refresh($refresh_token){
	}
	**/

	//获取当前登录用户的账户信息
	public function me(){
		$params=array();
		return $this->api('user', $params);
	}

	//获取最新动弹列表
	public function tweet_list($page=1, $page_size=20){
		$params=array(
			'client_id'=>$this->client_id,
			'pageSize'=>$page_size,
			'page'=>$page
		);
		return $this->api('tweet_list', $params);
	}

	//发布动弹
	public function tweet_pub($msg){
		$params=array(
			'msg'=>$msg
		);
		return $this->api('tweet_pub', $params);
	}

	//调用接口
	/**
	//示例：获取当前登录用户的账户信息
	$result=$osc->api('user', array(), 'GET');
	**/
	public function api($url, $params=array(), $method='GET'){
		$url=$this->api_url.$url;
		$params['access_token']=$this->access_token;
		$params['dataType']=$this->format;
		if($method=='GET'){
			$result=$this->http($url.'?'.http_build_query($params));
		}else{
			$result=$this->http($url, http_build_query($params), 'POST');
		}
		return $result;
	}

	//提交请求
	private function http($url, $postfields='', $method='GET', $headers=array()){
		$ci=curl_init();
		curl_setopt($ci, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ci, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ci, CURLOPT_TIMEOUT, 30);
		if($method=='POST'){
			curl_setopt($ci, CURLOPT_POST, TRUE);
			if($postfields!='')curl_setopt($ci, CURLOPT_POSTFIELDS, $postfields);
		}
		$headers[]='User-Agent: OSC.PHP(piscdong.com)';
		curl_setopt($ci, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ci, CURLOPT_URL, $url);
		$response=curl_exec($ci);
		curl_close($ci);
		$json_r=array();
		if($response!='')$json_r=json_decode($response, true);
		return $json_r;
	}
}
